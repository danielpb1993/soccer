package com.example.soccer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static String COUNTRY = "com.example.soccer.COUNTRY";
    private Spinner spinner;
    private ArrayList<Country> countries = new ArrayList<>();
    private CountryAdapter countryAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner);


        initCountries();

        countryAdapter = new CountryAdapter(getApplicationContext(), countries);
        spinner.setAdapter(countryAdapter);
        spinner.setSelection(0, true);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Country selectedItem = (Country) parent.getItemAtPosition(position);
                String selectedCountry = selectedItem.getName();
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(COUNTRY, selectedCountry);
                Toast.makeText(MainActivity.this, selectedCountry, Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initCountries() {
        countries.add(new Country("Argentina", R.drawable.argentina));
        countries.add(new Country("Australia", R.drawable.australia));
        countries.add(new Country("Canada", R.drawable.canada));
        countries.add(new Country("Congo", R.drawable.congo));
        countries.add(new Country("Egypt", R.drawable.egypt));
        countries.add(new Country("France", R.drawable.france));
        countries.add(new Country("Germany", R.drawable.germany));
        countries.add(new Country("Greece", R.drawable.greece));
        countries.add(new Country("India", R.drawable.india));
        countries.add(new Country("Indonesia", R.drawable.indonesia));
        countries.add(new Country("Ireland", R.drawable.ireland));
        countries.add(new Country("Italy", R.drawable.italy));
        countries.add(new Country("Kenya", R.drawable.kenya));
        countries.add(new Country("Mexico", R.drawable.mexico));
        countries.add(new Country("New Zeland", R.drawable.newzeland));
        countries.add(new Country("Norway", R.drawable.norway));
        countries.add(new Country("Poland", R.drawable.poland));
        countries.add(new Country("Qatar", R.drawable.qatar));
        countries.add(new Country("South Africa", R.drawable.southafrica));
        countries.add(new Country("South Korea", R.drawable.southkorea));
        countries.add(new Country("Spain", R.drawable.spain));
        countries.add(new Country("United Kingdom", R.drawable.uk));
        countries.add(new Country("United States of America", R.drawable.usa));
    }


}