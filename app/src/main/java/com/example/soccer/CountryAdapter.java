package com.example.soccer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CountryAdapter extends ArrayAdapter<Country> {
    public CountryAdapter (Context context, ArrayList<Country> countryArrayList) {
        super(context, 0, countryArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row,
                    parent,
                    false
            );
            ImageView imageView = convertView.findViewById(R.id.imageViewFlag);
            TextView textView = convertView.findViewById(R.id.textNameFlag);
            Country currentCountry = getItem(position);
            if (currentCountry != null) {
                imageView.setImageResource(currentCountry.getImageFlag());
                textView.setText(currentCountry.getName());
            }
        }
        return convertView;
    }
}
