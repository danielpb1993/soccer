package com.example.soccer;

import java.util.ArrayList;

public class League {
    String flag;
    String leagueTitle;
    String textDescription;
    String webSite;
    ArrayList<String> images = new ArrayList<>(4);

    public League() {
    }

    public League(String flag, String leagueTitle, String textDescription, String webSite, ArrayList<String> images) {
        this.flag = flag;
        this.leagueTitle = leagueTitle;
        this.textDescription = textDescription;
        this.webSite = webSite;
        this.images = images;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
