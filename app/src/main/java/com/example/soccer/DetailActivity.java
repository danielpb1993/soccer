package com.example.soccer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<League> leagues = new ArrayList<>();
    String JSON_URL = "";
    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        String selectedCountry = getIntent().getStringExtra(MainActivity.COUNTRY);
        String JSON_URL = String.format("https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=%s&s=Soccer", selectedCountry);
        recyclerView = findViewById(R.id.recyclerDetail);
        getLeagues(JSON_URL);


    }

    private void getLeagues(String JSON_URL) {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("countrys");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject leagueObject = ((JSONObject)jsonArray.get(i));
                                League league = new League();
                                league.setFlag(leagueObject.getString("strBadge"));
                                league.setLeagueTitle(leagueObject.getString("strLeague"));
                                league.setTextDescription(leagueObject.getString("strDescriptionEN"));
                                league.setWebSite(leagueObject.getString("strWebsite"));
                                if (!"null".equals(leagueObject.getString("strFanart1"))) {
                                    league.getImages().add(leagueObject.getString("strFanart1"));
                                    league.getImages().add(leagueObject.getString("strFanart2"));
                                    league.getImages().add(leagueObject.getString("strFanart3"));
                                    league.getImages().add(leagueObject.getString("strFanart4"));
                                }
                                leagues.add(league);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
                        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recycler_divider));
                        recyclerView.addItemDecoration(dividerItemDecoration);
                        myAdapter = new MyAdapter(getApplicationContext(), leagues);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }
}