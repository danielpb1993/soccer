package com.example.soccer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context mContext;
    private List<League> mLeague;

    public MyAdapter(Context mContext, List<League> mLeague) {
        this.mContext = mContext;
        this.mLeague = mLeague;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.recycler_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.textTitleDetail.setText(mLeague.get(position).getLeagueTitle());
        holder.textDescDetail.setText(mLeague.get(position).getTextDescription());
        Picasso.get().load(mLeague.get(position).getFlag())
                .fit()
                .centerCrop()
                .into(holder.imageDetail);

        holder.btVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mLeague.get(holder.getAdapterPosition()).getWebSite();
                if (!url.startsWith("https://")) {
                    url = "https://" + url;
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }

                mContext.startActivity(intent);
            }
        });

        if (mLeague.get(position).getImages().size() != 0) {
            holder.btImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext.getApplicationContext(), Images.class);
                    intent.putStringArrayListExtra("imageUrls", mLeague.get(holder.getAdapterPosition()).getImages());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    mContext.startActivity(intent);
                }
            });
        } else {
            holder.btImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "No images available for this league", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mLeague.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageDetail;
        TextView textTitleDetail;
        TextView textDescDetail;
        Button btVisit;
        Button btImages;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageDetail = itemView.findViewById(R.id.imageDetail);
            textTitleDetail = itemView.findViewById(R.id.textTitleDetail);
            textDescDetail = itemView.findViewById(R.id.textDescDetail);
            btVisit = itemView.findViewById(R.id.btVisit);
            btImages = itemView.findViewById(R.id.btImages);
        }
    }
}
