package com.example.soccer;

public class Country {
    private String name;
    private int imageFlag;

    public Country() {
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageFlag() {
        return imageFlag;
    }

    public void setImageFlag(int imageFlag) {
        this.imageFlag = imageFlag;
    }

    public Country(String name, int imageFlag) {
        this.name = name;
        this.imageFlag = imageFlag;
    }
}
